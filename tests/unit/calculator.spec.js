import { expect } from 'chai';
import calculator from '@/libs/calculator';

describe('calculator', () => {
  it('calculate aditions', () => {
    const calc = ['15', '+', '5'];
    const result = '20';
    expect(calculator(calc)).to.be.equal(result);
  });
  it('calculate substractions', () => {
    const calc = ['15', '-', '5'];
    const result = '10';
    expect(calculator(calc)).to.be.equal(result);
  });
  it('calculate multiplications', () => {
    const calc = ['15', '+', '5', 'x', '2'];
    const result = '25';
    expect(calculator(calc)).to.be.equal(result);
  });
  it('calculate divisions', () => {
    const calc = ['15', 'x', '2', '+', '6', '/', '3'];
    const result = '32';
    expect(calculator(calc)).to.be.equal(result);
  });
});
